    $  startx

    $ ls -l   (Если нет каталогов то:)
    $ pwd

    $ xdg-user-dirs-update
    $ ls -la
    $ clear

    $ sudo loadkeys es
    $ locale -a

    $ sudo nano /etc/X11/xorg.conf.d/00-keyboard.conf
--
    {
    Section "InputClass"
     Identifier "system-keyboard"
     MatchlsKeyboard "on"
     Option "XkbLayout" "us,ru"
     Option "XkbModel" "ps105"
     Option "XkbOptions" "grp:alt_shift_toggle"
    EndSection
    }
--
    (Что бы ни в водить в ручную все это: при запуске скрипта это устанавливается автоматом)
    
    $ locale -a
--------------------------------------------------------------------
    $ sudo nano /etc/locale.conf
--
    {
    LANG=es_ES.UTF-8
    }
--
    cat /etc/locale.conf (показать файлл)

    (При надобности меняем: )
--
    {
    LANG=ru_RU.UTF-8
    }
--
------------------------------------------------------------------------

