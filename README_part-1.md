# hyperbola_0.4_part-1_Hyperbola-install

    loadkeyys us

**Если просто интернет:**

    pacman -Syu
    clear

    cfdisk   (выбираем dos)
    И странная разметка: (это для примера) 500М boot, 23G root, 15G home, 2G swap
    clear

   **Форматируем разделы:**
    mkfs -t ext2 /dev/sda1 
    mkfs -t ext4 /dev/sda2 
    mkfs -t ext4 /dev/sda3 
    mkswap /dev/sda4 
    swapon /dev/sda4 
    clear

    mount /dev/sda2 /mnt
    mkdir /mnt/boot
    mkdir /mnt/home
    mount /dev/sda1 /mnt/boot
    mount /dev/sda3 /mnt/home
    clear

    pacman -Sy hyperbola-keyring
    clear

    pacstrap /mnt base base-devel
    pacstrap /mnt grub-bios
    pacstrap /mnt dhcpcd
    pacstrap /mnt xenocara-input-synaptics
    clear

    genfstab -U -p /mnt >> /mnt/etc/fstab
    arch-chroot /mnt
    nano /etc/hostname (gnulinuxlibre) (имя host файла может быть любое)
    ln -sv /usr/share/zoneinfo/Europe/Minsk /etc/localtime   (Место положение)
    nano /etc/locale.gen (Выбираем локаль, язык системы)
    locale-gen

    nano /etc/vconsole.conf (KEYMAP=us)
    grub-install /dev/sda
    grub-mkconfig -o /boot/grub/grub.cfg
    mkinitcpio -p linux-libre-lts
    grub-mkconfig -o /boot/grub/grub.cfg
    passwd admin(x2)
    exit

    umount /mnt/{boot,home,}
    reboot

**Далее водим рот и пароль от рута:**

    loadkeys us
    rc-status

**Если мы хотим знать все:**

    rc-status --all
    clear

    rc-service dhcpcd start
    rc-update add dhcpcd default
    clear

    useradd -m -g users  -G wheel -g users -s /bin/bash admin
    passwd admin (x2)

    useradd -m -g users -G audio,disk,games,http,input,lp,network,optical,power,scanner,storage,sys,video,wheel -g users -s /bin/bash user
    passwd user (x2)

    gpasswd -d user wheel   (Убираем из учетной записи user - wheel "для безопасности")
    passwd -l root

    clear
    reboot
    
**Заходим от admin:**

    $ doas loadkeys us
    $ clear

    $ doas nano /etc/conf.d/keymaps (что бы это строка выгледилабы так: keymaps="us")
    $ reboot

    $ groups   (просто смотрим или все правильно)
    $ doas pacman -Syu

                                                       
                                                       
                                  
                  *  Для того чтобы выключать и перезагружать комп из учетной записи "user":
                                          В файле: /etc/doas.conf
                                      До писываем две строчки в конце:
                  В итоге можем перезагружать и выключать компьютер от этого пользователя,
                             вводя doas reboot и doas poweroff соответственно.*
               
    $ doas nano /etc/doas.conf
--
    permit nopass user as root cmd reboot
    permit nopass user as root cmd poweroff
--


                                             Заметки:
                                                                
                   group user     (посмотреть какие группы добавлены у пользователя)
                   gpasswd -d user wheel     (убрать у пользователя к примеру admin группу wheel)
                   passwd -l root    (от админа - отключить "root")
                                            
                   gpasswd -a admin wheel   (Добавить в учетную запись "admin" "wheel")
                                            
                   При надобности можно установить и настроить "wpa_supplicant"
                            # pacman -S wpa_supplicant wpa-supplicant-gui