# Установка Telegram
**От учетной записи "admin":**

    elinks https://telegram.org/dl/desktop/linux
    
    *Перед началом проверть версию телеграм и при надобности измените цифры:*
     *Версия будет указана после в вода команды выше, поймете!*
    
    tar -xf tsetup.3.7.3.tar.xz
    doas cp -r Telegram /etc/local/
    cd /etc/local/Telegram/
    ls
    exit

**Заходим  с учетной записи  "user"**

*Далее заходим в меню openbox и запускаем телеграм, после этого и в rofi  тоже должен появится значек, все :)*